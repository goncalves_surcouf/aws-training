class UrlMappings {

    static mappings = {
        "/status"(controller: "status")
        "/upload/$site/$source"(controller: 'upload', action: 'upload')
    }
}
