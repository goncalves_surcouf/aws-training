package aws.training

import grails.transaction.Transactional

import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.PutObjectRequest
import com.amazonaws.services.s3.model.PutObjectResult
import org.codehaus.groovy.grails.commons.GrailsApplication
import grails.plugin.awssdk.AmazonWebService
import org.springframework.web.multipart.MultipartFile

import javax.annotation.PostConstruct


class StorageService {

    static transactional = false

    static final Integer READ_BUFFER_SIZE = 8192

    GrailsApplication grailsApplication

    FileYardService fileYardService

    AmazonWebService amazonWebService

    private String bucketName
    private String cacheControlPragma

    @PostConstruct
    def init() {
        this.bucketName = grailsApplication.config.aws.s3.bucket.photo.landing
        this.cacheControlPragma = grailsApplication.config.aws.s3.bucket.cacheControl
    }

    File toLocalDisk(MultipartFile multipartFile) {
        File destFile = new File(grailsApplication.config.upload.landing.directory + '/' + multipartFile.originalFilename)
        multipartFile.transferTo(destFile)
        destFile
    }



    Boolean toS3Bucket(File localFile, String cuRefDirectory, String picHash) {

        String s3Key = localFile.name

        if (cuRefDirectory) {
            s3Key = "${cuRefDirectory}/${localFile.name}"
        }

        def metadata = new ObjectMetadata()
        metadata.setCacheControl(cacheControlPragma)

        def putObjectRequest = new PutObjectRequest(bucketName, s3Key, localFile)

        putObjectRequest.setMetadata(metadata)

        PutObjectResult putObjectResult= amazonWebService.s3.putObject(putObjectRequest)

        return putObjectResult.contentMd5 == picHash + '=='

    }

}
