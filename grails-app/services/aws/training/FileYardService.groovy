package aws.training

import grails.transaction.Transactional
import java.security.MessageDigest

class FileYardService {

    static transactional = false

    static final Integer READ_BUFFER_SIZE = 8192

    Integer getSize(File file) {
        file.size()
    }

    String calculateHash(File fileToHash) {

        log.trace("Hashing '${fileToHash}'")

        MessageDigest digest = MessageDigest.getInstance('MD5')

        fileToHash.eachByte(READ_BUFFER_SIZE) { buffer, bufferSize ->
            digest.update(buffer, 0, bufferSize)
        }

        // Removing trailing '==' to be compliant with Perl alike hash used in C&B legacy Perl scripts
        digest.digest().encodeBase64().toString() - '=='
    }
}

