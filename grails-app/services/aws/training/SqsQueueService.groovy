package aws.training

import com.amazonaws.services.sqs.model.SendMessageRequest
import grails.converters.JSON
import grails.plugin.awssdk.AmazonWebService
import org.codehaus.groovy.grails.commons.GrailsApplication


class SqsQueueService {

    static transactional = false

    GrailsApplication grailsApplication

    AmazonWebService amazonWebService

    void sendToCheckQueue(Map messageBody) {
        sendToQueue(messageBody, grailsApplication.config.aws.sqs.queue.photo.check)
    }

    private void sendToQueue(Map messageBody, String queue) {
        messageBody.photo.date=new Date()
        String jsonMessage = messageBody as JSON
        log.trace("Sending ${jsonMessage} to ${queue}")
        amazonWebService.sqs.sendMessage(new SendMessageRequest(queue, jsonMessage))
    }

}
