package aws.training

import grails.converters.JSON
import org.codehaus.groovy.grails.commons.GrailsApplication

class StatusController {

    GrailsApplication grailsApplication

    def index() {
        def resp = [
                name: grailsApplication.metadata['app.name'],
                version: grailsApplication.metadata['app.version']
        ]

        log.info('[status:ok]')



        render(status: 200, contentType: 'application/json', text: resp as JSON)
    }

}
