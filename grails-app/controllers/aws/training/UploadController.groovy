package aws.training

import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import aws.training.logs.LogUtils
import aws.training.constants.AllowedSite
import aws.training.constants.AllowedSource

class UploadController {

    static allowedMethods = [upload: 'POST']

    StorageService storageService

    FileYardService fileYardService

    SqsQueueService sqsQueueService

    static Integer PICTURE_MAX_SIZE = 1024 * 1024 * 10;//10MB





    def upload() {
        String correlationId = LogUtils.newCorrelationId()

        log.trace('Beginning validity check')

        if (!AllowedSite.contains(params.site)) {
            log.trace('Wrong site detected, returning 400')
            return render(status: 400, text: message(code: 'http.400.wrong.site'))
        }

        if (!AllowedSource.contains(params.source)) {
            log.trace('Wrong source detected, returning 400')
            return render(status: 400, text: message(code: 'http.400.wrong.source'))
        }

        if (AllowedSource.WEB.matches(params.source) && (params.cu_ref || params.pr_ref)) {
            log.trace('Non-empty cu_ref and/or pr_ref on WEB upload, returning 400')
            return render(status: 400, text: 'When uploading from WEB cu_ref and pr_ref must be empty')
        }

        if (AllowedSource.EDI.matches(params.source) && (!params.cu_ref || !params.pr_ref)) {
            log.trace('Empty cu_ref and/or pr_ref on EDI upload, returning 400')
            return render(status: 400, text: 'When uploading from EDI cu_ref and pr_ref must not be empty')
        }

        File localFile

        try {
            MultipartFile multipartFile = (request as MultipartHttpServletRequest).getFile('image')

            if (multipartFile.empty) {
                log.trace("'image' not found in POST body, returning 400")
                return render(status: 400, text: 'Expected "image" field in POST not found.')
            }

            if (multipartFile.size > PICTURE_MAX_SIZE) {
                log.warn("Impossible de traiter la photo ${localFile.name}. La taille est trop grande (> ${PICTURE_MAX_SIZE})")
                render(status: 413)
            }

            localFile = storageService.toLocalDisk(multipartFile)

            log.info("Received file '${localFile.name}', ${params}")
            def picHash = fileYardService.calculateHash(localFile)
            if (storageService.toS3Bucket(localFile, params.cu_ref, picHash)) {

                sqsQueueService.sendToCheckQueue([
                        photo: [
                                correlationId: correlationId,
                                site: params.site,
                                source: params.source,
                                name: localFile.name,
                                size: fileYardService.getSize(localFile),
                                hash: picHash,
                                cuReference: params.cu_ref,
                                prReference: params.pr_ref
                        ]
                ])

                log.info("File '${localFile.name}' sent to Landing Bucket, and message sent to Check Queue ($params)")

                render(status: 200)
            } else {

                log.warn("File '${localFile.name}'  not properly sent to Landing Bucket, local and remote SHA1 differ, returning 500 ($params)")
                render(status: 500)
            }
        } catch (Exception e) {
            log.error("Upload error for ${params}", e)
            render(status: 500)
        } finally {
            if (localFile && localFile.exists()) {
                log.trace("End of upload processing, cleaning local file '${localFile.name}'")
                localFile.delete()
            }
        }
    }
}
