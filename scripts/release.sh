#!/bin/sh

if [ -z "$NEW_VERSION" ]; then
    echo "ERROR: Variable NEW_VERSION must be set"
    exit 1
fi

APP_NAME="training"

echo "===> Testing"
rm -rf target
./grailsw test-app unit: --non-interactive
if [ $? -ne 0 ]; then
    echo "ERROR: Tests failed"
    exit 1
fi
rm -rf target


echo "===> Upgrading version number to $NEW_VERSION"

sed -i "" "s/^app.version=.*/app.version=$NEW_VERSION/" application.properties

echo "===> Building war"
./grailsw war --non-interactive

if [ $? -ne 0 ]; then
    echo "ERROR: Building war fail"
    git reset --hard HEAD~
    exit 1
fi

git commit -am "upgrade version number to $NEW_VERSION"

S3DEV=s3://applicationtraining.dev
echo "===> Publishing war to $S3DEV"
/Users/Shared/Jenkins/apps/s3cmd-1.5.0-alpha1/s3cmd put target/*.war $S3DEV/$APP_NAME/$NEW_VERSION/ -c /Users/Shared/Jenkins/.s3cfg

if [ $? -ne 0 ]; then
    echo "ERROR: Pushing to $S3DEV failed"
    git reset --hard HEAD~
    exit 1
fi

S3PROD=s3://applicationtraining.prod
echo "===> Publishing war to $S3PROD..."
/Users/Shared/Jenkins/apps/s3cmd-1.5.0-alpha1/s3cmd put target/*.war $S3PROD/$APP_NAME/$NEW_VERSION/ -c /Users/Shared/Jenkins/.s3cfg

if [ $? -ne 0 ]; then
    echo "ERROR: Pushing to $S3PROD failed"
    git reset --hard HEAD~
    exit 1
fi

