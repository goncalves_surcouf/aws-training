package aws.training.constants

/**
 * Created by cgoncalves on 28/05/2014.
 */
public enum AllowedSource {
    WEB, EDI, SCAN, MAIL, CATALOGUE

    static Boolean contains(String source) {
        values().collect { it.name() }.contains(source.toUpperCase())
    }

    public matches(String toMatch) {
        this.name() == toMatch.toUpperCase()
    }

}