package aws.training.constants

/**
 * Created by cgoncalves on 28/05/2014.
 */
public enum AllowedSite {

    LC, AB, LCDN

    static Boolean contains(String site) {
        values().collect { it.name() }.contains(site.toUpperCase())
    }

}