package aws.training.logs

import org.slf4j.MDC

/**
 * Created by cgoncalves on 28/05/2014.
 */
class LogUtils {

    public static final String CORRELATION_ID_KEY = "correlationId"

    static String getCorrelationId() {
        String uuid = MDC.get(CORRELATION_ID_KEY)
        if (uuid == null) {
            uuid = UUID.randomUUID()
            MDC.put(CORRELATION_ID_KEY, uuid)
        }
        uuid
    }

    static String newCorrelationId() {
        String uuid = UUID.randomUUID()
        MDC.put(CORRELATION_ID_KEY, uuid)
        uuid
    }
}
